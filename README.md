# glog_override
For when you want GTK to shut the hell up

##What does it do?
This cutting-edge state-of-the-art piece of engineering
suppresses annoying log messages created by gtk.

##Usage
To start using this thing you need to get familiar with LD_PRELOAD
environmental variable. You can either set it on each gtk program
invocation `LD_PRELOAD=/path/to/libglog_override.so gedit` or
set it in your `.bashrc` for example to not have to prepend each run of
gtk program with that long annoying line.
