#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <subhook.h>

#define __USE_GNU 1

#include <dlfcn.h>
#include <stdbool.h>

#include "library.h"

#define NO_GLIB ((void *) 1)

static void ourg_logv(const gchar *log_domain, GLogLevelFlags log_level, const gchar *format, va_list args);

static int block_level = G_LOG_LEVEL_WARNING;

static void (*g_logv_o)(const gchar *log_domain, GLogLevelFlags log_level, const gchar *format, va_list args) = NULL;

static subhook_t log_hook = NULL;

static int (*g_application_run_o)(GApplication *application, int argc, char **argv) = NULL;

static void (*gtk_init_o)(int *argc, char ***argv) = NULL;

static gboolean (*gtk_init_check_o)(int *argc, char ***argv) = NULL;

static gboolean (*gtk_init_with_args_o)(gint *argc, gchar ***argv, const gchar *parameter_string,
										const GOptionEntry *entries, const gchar *translation_domain,
										GError **error) = NULL;

void (*g_set_application_name_o)(const gchar *application_name) = NULL;


/*
 * Our entry point
 * To hook library's function we have to first make sure it's already loaded
 * We abuse LD_PRELOAD to do that and then wait for call to a known function from libgtk-3
 * that is used somewhere in the beginning and preferably only once
 * Then we just do our stuff and pass the call further on
 */
static void entry(void) {
	static bool init = FALSE;


	char *env;

	if(!init) {
		init = TRUE;
		g_application_run_o = dlsym(RTLD_NEXT, "g_application_run");
		gtk_init_o = dlsym(RTLD_NEXT, "gtk_init");
		gtk_init_check_o = dlsym(RTLD_NEXT, "gtk_init_check");
		gtk_init_with_args_o = dlsym(RTLD_NEXT, "gtk_init_with_args");
		g_set_application_name_o = dlsym(RTLD_NEXT, "g_set_application_name");
		g_logv_o = dlsym(RTLD_DEFAULT, "g_logv");
/*		if(g_logv_o == NULL ||
		   g_application_run_o == NULL ||
		   g_application_run_o == g_application_run) {
			g_application_run_o = NO_GLIB;
		}
*/
		env = getenv("G_BLOCK");
		if(env != NULL) {
			if(strcmp(env, "CRIT") == 0) {
				block_level = G_LOG_LEVEL_CRITICAL;
			}
			else if(strcmp(env, "WARN") == 0) {
				block_level = G_LOG_LEVEL_WARNING;
			}
			else if(strcmp(env, "NONE") == 0) {
				block_level = G_LOG_LEVEL_DEBUG;
			}
		}

		log_hook = subhook_new(g_logv_o, ourg_logv, 0);
		subhook_install(log_hook);
		//g_logv_t = subhook_get_trampoline(log_hook); //is broken
	}

}

int g_application_run(GApplication *application, int argc, char **argv) {
	entry();
	return g_application_run_o(application, argc, argv);
}

void gtk_init(int *argc, char ***argv) {
	entry();
	gtk_init_o(argc, argv);
}

gboolean gtk_init_check(int *argc, char ***argv) {
	entry();
	return gtk_init_check_o(argc, argv);
}

gboolean gtk_init_with_args(gint *argc, char ***argv, const gchar *parameter_string, const GOptionEntry *entries,
							const gchar *translation_domain, GError **error) {
	entry();
	return gtk_init_with_args_o(argc, argv, parameter_string, entries, translation_domain, error);
}
void g_set_application_name(const gchar *application_name) {
	entry();
	g_set_application_name_o(application_name);
}

static void ourg_logv(const gchar *log_domain, GLogLevelFlags log_level, const gchar *format, va_list args) {
	if(log_hook != NULL && (log_level & ~3) < block_level) {
		subhook_remove(log_hook);
		g_logv_o(log_domain, log_level, format, args);
		subhook_install(log_hook);
	}
}