#ifndef GLOG_OVERRIDE_LIBRARY_H
#define GLOG_OVERRIDE_LIBRARY_H

#define GLIB_COMPILATION 1
#define __GIO_GIO_H_INSIDE__ 1

#include <glib/gmacros.h>
#include <glib/gtypes.h>
//#include <gio/gapplication.h>
#include <gio/giotypes.h>

struct _GApplication {
	/*< private >*/
	GObject parent_instance;

	void *priv;
};
typedef struct _GApplication GApplication;

#endif